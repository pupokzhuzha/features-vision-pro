package com.Features.Vision.Pro.content.app.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.Features.Vision.Pro.content.app.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}