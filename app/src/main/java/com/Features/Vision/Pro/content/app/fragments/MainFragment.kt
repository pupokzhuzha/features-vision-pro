package com.Features.Vision.Pro.content.app.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.Features.Vision.Pro.content.app.R
import com.Features.Vision.Pro.content.app.databinding.FragmentMainBinding
import com.Features.Vision.Pro.content.app.databinding.FragmentStartBinding

class MainFragment : Fragment() {

    private var binding: FragmentMainBinding? = null
    private val mBinding get() = binding!!

    private var pageCount = 1

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMainBinding.inflate(layoutInflater, container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mBinding.apply {
            back.setOnClickListener {
                activity?.onBackPressedDispatcher?.onBackPressed()
            }
            next.setOnClickListener {
                visibilityAllGone()
                pageCount++

                when(pageCount){
                    2 -> second.visibility = View.VISIBLE
                    3 -> third.visibility = View.VISIBLE
                    4 -> forth.visibility = View.VISIBLE
                    5 -> fifth.visibility = View.VISIBLE
                    6 -> six.visibility = View.VISIBLE
                    7 -> seven.visibility = View.VISIBLE
                    8 -> eight.visibility = View.VISIBLE
                    9 -> nine.visibility = View.VISIBLE
                    10 -> ten.visibility = View.VISIBLE
                    11 -> eleven.visibility = View.VISIBLE
                    12 -> twelve.visibility = View.VISIBLE
                    13 -> thirteen.visibility = View.VISIBLE
                    14 -> fourteen.visibility = View.VISIBLE
                    15 -> fifteen.visibility = View.VISIBLE
                    16 -> sixteen.visibility = View.VISIBLE
                    17 -> seventeen.visibility = View.VISIBLE
                    18 -> {
                        next.isEnabled = false
                        enabled.visibility = View.VISIBLE
                        eighteen.visibility = View.VISIBLE
                    }
                }
            }
        }
    }

    private fun visibilityAllGone(){
        mBinding.apply {
            first.visibility = View.GONE
            second.visibility = View.GONE
            third.visibility = View.GONE
            forth.visibility = View.GONE
            fifth.visibility = View.GONE
            six.visibility = View.GONE
            seven.visibility = View.GONE
            eight.visibility = View.GONE
            nine.visibility = View.GONE
            ten.visibility = View.GONE
            eleven.visibility = View.GONE
            twelve.visibility = View.GONE
            thirteen.visibility = View.GONE
            fourteen.visibility = View.GONE
            fifteen.visibility = View.GONE
            sixteen.visibility = View.GONE
            seventeen.visibility = View.GONE
            eighteen.visibility = View.GONE
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}